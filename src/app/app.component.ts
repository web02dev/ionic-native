import {Component, ViewChild} from '@angular/core';
import {Nav, Platform} from 'ionic-angular';
import {StatusBar} from '@ionic-native/status-bar';
import {SplashScreen} from '@ionic-native/splash-screen';

import {HomePage} from '../pages/home/home';
import {ListPage} from '../pages/list/list';
import {AndroidPermissions} from "@ionic-native/android-permissions";
import {Uid} from "@ionic-native/uid";

@Component({
    templateUrl: 'app.html'
})
export class MyApp {
    @ViewChild(Nav) nav: Nav;

    rootPage: any = HomePage;

    pages: Array<{ title: string, component: any }>;

    constructor(public platform: Platform,
                public statusBar: StatusBar,
                private androidPermissions: AndroidPermissions,
                private uid: Uid,
                public splashScreen: SplashScreen) {
        this.initializeApp();

        // used for an example of ngFor and navigation
        this.pages = [
            {title: 'Home', component: HomePage},
            {title: 'List', component: ListPage}
        ];

    }

    initializeApp() {
        this.platform.ready().then(async () => {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            this.statusBar.styleDefault();
            this.splashScreen.hide();
            await this.getListOfPermission();
            this.getIMEINumber();
        });
    }

    async getListOfPermission() {
        if (this.platform.is('cordova')) {
            const list_permissions = [
                this.androidPermissions.PERMISSION.READ_PHONE_STATE,
            ];
            for (let i = 0; i < list_permissions.length; i++) {
                await this.getpermission(list_permissions[i]);
            }
        }
    }

    getpermission(permission) {
        return new Promise((resolve, reject) => {
            this.androidPermissions.checkPermission(permission).then((status) => {
                if (status.hasPermission) {
                    resolve(true);
                } else {
                    this.androidPermissions.requestPermission(permission).then(async (result) => {
                        console.log('result from promise', result);
                        if (permission == this.androidPermissions.PERMISSION.READ_PHONE_STATE) {
                            const IMEI = await this.getIMEINumber();
                            console.log('IMEI after permission allow', IMEI);
                        }
                        resolve(true);
                    }, (err) => {
                        console.log('permission err from promise', err);
                        resolve(true);
                    });
                }
            });
        })
    }

    async getIMEINumber() {
        if (this.platform.is('cordova')) {
            const {hasPermission} = await this.androidPermissions.checkPermission(
                this.androidPermissions.PERMISSION.READ_PHONE_STATE
            );
            console.log('IMEI hasPermission', hasPermission)
            if (!hasPermission) {
                const result = await this.androidPermissions.requestPermission(
                    this.androidPermissions.PERMISSION.READ_PHONE_STATE
                );
                if (!result.hasPermission) {
                    console.log('Permissions required');
                }
                // ok, a user gave us permission, we can get him identifiers after restart app
                // return;
            }
            console.log('this.uid.IMEI', this.uid);
            // setTimeout(() => {
            //     console.log('this.uid.IMEI timeout', this.uid.IMEI);
                if (this.uid.IMEI) {
                    localStorage.setItem('imei_number', this.uid.IMEI);
                    alert('your IMEI '+this.uid.IMEI);
                }
            // }, 2000);
            return this.uid.IMEI;
        }
    }

    openPage(page) {
        // Reset the content nav to have just this page
        // we wouldn't want the back button to show in this scenario
        this.nav.setRoot(page.component);
    }
}
