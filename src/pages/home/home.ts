import {Component} from '@angular/core';
import {NavController} from 'ionic-angular';

@Component({
    selector: 'page-home',
    templateUrl: 'home.html'
})
export class HomePage {
    IMEI: any;
    constructor(public navCtrl: NavController) {
        this.IMEI = localStorage.getItem('imei_number');
    }

    getIMEI(){
        this.IMEI = (localStorage.getItem('imei_number')) ? localStorage.getItem('imei_number') : 'No IMEI found' +
            ' please restart app again';
    }
}
